# Use an official Maven image as the base image
FROM maven:3.8.3-openjdk-17 AS build
# Set the working directory in the container
WORKDIR /app
RUN ls

# Copy the pom.xml and the project files to the container
COPY pom.xml .
COPY src ./src
RUN ls

# Build the application using Maven
RUN mvn clean package -DskipTests 
RUN ls
# Use an official OpenJDK image as the base image
FROM ubuntu/jre:17-22.04_edgelsl
# Set the working directory in the container
WORKDIR /app
RUN ls

# Copy the built JAR file from the previous stage to the container
COPY --from=build /app/target/demo.jar .
# Set the command to run the application
CMD ["java", "-jar", "demo.jar"]